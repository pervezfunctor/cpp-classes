#include "combinatorics.hpp"


namespace seartipy {
    bigint factorial(bigint n)
    {
        bigint fact = 1;
        for(bigint i = 2; i <= n; ++i)
            fact *= i;
        return fact;
    }
    
    bigint ncr(bigint n, bigint r)
    {
        bigint num = 1, den = 1;
        r = std::min(r, n - r);
        for(auto i = 1; i <= r; ++i, --n) {
            num *= n;
            den *= i;
        }
        return num / den;
    }
    
    std::vector<bigint> pascal_line(bigint n)
    {
        std::vector<bigint> pl;
        for (auto i = 0; i <= n; ++i)
            pl.push_back(ncr(n, i));
        return pl;
    }
    std::vector<std::vector<bigint>> pascal_triangle(bigint n)
    {
        std::vector<std::vector<bigint>> pl;
        for (auto i = 0; i <= n; ++i)
            pl.push_back(pascal_line(i));
        return pl;
    }
}