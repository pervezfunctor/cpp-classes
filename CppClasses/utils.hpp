#pragma once

#include"includes.hpp"

namespace seartipy {
    std::vector<std::string> split(std::string line);
    
    int gcd(int, int);
    
    inline void verify(bool b) {
        if(!b)
            throw std::runtime_error("VERIFY FATAL ERROR");
    }
    
    template<typename Cont>
    void print(Cont const& c, char const * delim = ", ", char const* start = "[ ", char const* end = " ]\n")
    {
        std::cout << start;
        auto count = 0;
        for(auto const& e : c) {
            std::cout << e;
            if(++count != c.size())
                std::cout << delim;
        }
        std::cout << delim;
    }

}
