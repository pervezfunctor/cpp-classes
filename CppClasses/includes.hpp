#pragma once

#include <cmath>
#include <cassert>
#include <cstdint>

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>
#include <sstream>

#include <string>
#include <tuple>
#include <regex>

#include <vector>
#include <deque>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>

#include <memory>
#include <type_traits>

#include <boost/operators.hpp>
#include <boost/optional.hpp>

#include <range/v3/all.hpp>
