#include "includes.hpp"

using Char = char;
using Word = std::string;
using Line = std::string;

//boost::optional<Char> character(std::istream&);
//boost::optional<Word> word(std::istream& in);
//boost::optional<Line> line(std::istream& in);

inline bool character(std::istream& in, Char& ch)
{
    return static_cast<bool>(in.get(ch));
}

inline bool word(std::istream& in, Word& word)
{
    return static_cast<bool>(in >> word);
}

inline bool line(std::istream& in, Line& line)
{
    return static_cast<bool>(std::getline(in, line));
}

using CharList = std::vector<Char>;
using WordList = std::vector<Word>;
using LineList = std::vector<Line>;

CharList chars(std::istream& in);
WordList words(std::istream& in);
LineList lines(std::istream& in);

struct NumberedLine { int no; std::string line; };

std::vector<NumberedLine> numbered_lines(std::istream&);

using WordFreqMap = std::map<Word, int>;
using LineNosList = std::vector<int>;
using Index = std::map<Word, LineNosList>;

std::vector<NumberedLine> grep(std::istream& in, std::regex pattern);
WordFreqMap wordFreq(std::istream& in);
Index indexer(std::istream& in);
