#pragma once

#include "includes.hpp"

namespace seartipy {

    struct Point { int x; int y; };
    
    struct Size { int width; int height; };
    
    struct Rectangle { Point top; Size size; };
    
    struct Circle { Point center; int radius; };
    
    struct Shape {
        enum { RECTANGLE, CIRCLE } type;
        union {
            Rectangle rectangle;
            Circle circle;
        } value;
        
        Shape(Rectangle const& r) : type(RECTANGLE) {
            value.rectangle = r;
        }
        
        Shape(Circle const& c) : type(CIRCLE) {
            value.circle = c;
        }
    };

    inline bool operator==(Point const& pt, Point const& pt2)
    {
        return pt.x == pt2.x && pt.y == pt2.y;
    }
    
    inline bool operator!=(Point const& pt, Point const& pt2)
    {
        return !(pt == pt2);
    }
    
    inline Point offset(Point const& pt, int dx, int dy)
    {
        return Point { pt.x + dx, pt.y + dy };
    }
    
    inline double distance(Point const& pt)
    {
        return std::sqrt(pt.x * pt.x + pt.y * pt.y);
    }
    
    inline bool operator==(Size const& sz1, Size const& sz2)
    {
        return sz1.width == sz2.width;
    }
    
    inline bool operator!=(Size const& sz1, Size const& sz2)
    {
        return !(sz1 == sz2);
    }

    inline Size resize(Size const& sz, int dx, int dy)
    {
        return Size { sz.width + dx, sz.height + dy };
    }
    
    inline Rectangle offset(Rectangle const& r, int dx, int dy) {
        return Rectangle { offset(r.top, dx, dy), r.size };
    }
    
    inline Circle offset(Circle const& c, int dx, int dy) {
        return Circle { offset(c.center, dx, dy), c.radius };
    }
    
    inline Shape offset(Shape shp, int dx, int dy)
    {
        if(shp.type == Shape::CIRCLE)
            return offset(shp.value.circle, dx, dy);
        return offset(shp.value.rectangle, dx, dy);
    }
    
    inline Rectangle resize(Rectangle const& r, int dx, int dy)
    {
        return Rectangle { r.top, resize(r.size, dx, dy) };
    }
    
    inline Circle resize(Circle const& c, int dx, int dy)
    {
        assert(dx == dy);
        return Circle { c.center, c.radius + dx / 2 };
    }
    
    inline Shape resize(Shape const& shp, int dx, int dy)
    {
        if(shp.type == Shape::CIRCLE)
            return resize(shp.value.circle, dx, dy);
        return resize(shp.value.rectangle, dx, dy);
    }

    inline bool operator==(Rectangle const& r1, Rectangle const& r2)
    {
        return r1.top == r2.top && r1.size == r2.size;
    }
    
    inline bool operator!=(Rectangle const& r1, Rectangle const& r2)
    {
        return !(r1 == r2);
    }

    inline bool operator==(Circle const& c1, Circle const& c2)
    {
        return c1.center == c2.center && c1.radius == c2.radius;
    }
    
    inline bool operator!=(Circle const& c1, Circle const& c2)
    {
        return !(c1 == c2);
    }

    inline bool operator==(Shape const& shp1, Shape const& shp2)
    {
        if(shp1.type != shp2.type)
            return false;
        if(shp1.type == shp2.type)
        if(shp1.type == Shape::RECTANGLE)
            return shp1.value.rectangle == shp1.value.rectangle;
        return shp1.value.circle == shp2.value.circle;
    }
    
    inline bool operator!=(Shape const& shp1, Shape const& shp2)
    {
        return !(shp1 == shp2);
    }
}

