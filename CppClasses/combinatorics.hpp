#pragma once

#include "includes.hpp"

namespace seartipy {
    using bigint = int64_t;
    
    bigint factorial(bigint);
    bigint ncr(bigint, bigint);
    std::vector<bigint> pascal_line(bigint);
    std::vector<std::vector<bigint>> pascal_triangle(bigint);
}