#include "file_utils.hpp"

#include "utils.hpp"

using namespace std;

namespace seartipy {
    
    CharList chars(istream& in)
    {
        CharList v;
        char ch;
        while(character(in, ch))
            v.push_back(ch);
        return v;
    }
  
    WordList words(istream& in)
    {
        WordList v;
        string w;
        while(word(in, w))
            v.push_back(w);
        return v;
    }
    
    LineList lines(istream& in)
    {
        LineList v;
        string l;
        while(line(in, l))
            v.push_back(l);
        return v;
    }
    
    vector<NumberedLine> numbered_lines(istream& in)
    {
        vector<NumberedLine> v;
        auto lineNo = 0;
        for(auto line : lines(in))
            v.push_back(NumberedLine { ++lineNo, line });
        return v;
    }
    
    vector<NumberedLine> grep(istream& in, regex pattern)
    {
        vector<NumberedLine> result;
        for(auto pair : numbered_lines(in))
            if(regex_match(pair.line, pattern))
                result.push_back(pair);
        return result;
    }

    WordFreqMap wordFreq(istream& in)
    {
        WordFreqMap freq;
        for(auto word : words(in))
            ++freq[word]; // magic?
        return freq;
    }
    
    Index indexer(istream& in)
    {
        Index index;
        for(auto pair : numbered_lines(in))
            for(auto word : split(pair.line))
                index[word].push_back(pair.no);
        return index;
    }
}
