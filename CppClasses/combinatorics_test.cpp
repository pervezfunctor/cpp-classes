#include "catch.hpp"

#include "combinatorics.hpp"

using namespace seartipy;

TEST_CASE("factorial", "factorial")
{
    REQUIRE( factorial(0) == 1 );
    REQUIRE( factorial(1) == 1 );
    REQUIRE( factorial(5) == 120 );
}

TEST_CASE("ncr", "ncr")
{
    REQUIRE( ncr(0, 0) == 1 );
    REQUIRE( ncr(100, 0) == 1 );
    REQUIRE( ncr(1000, 1000) == 1);
    REQUIRE( ncr(100, 1) == 100 );
    REQUIRE( ncr(7, 3) == ncr(7, 4) );
    REQUIRE( ncr(5, 3) == 10 );
}

TEST_CASE("pascal_line", "pascal_line")
{
    auto zero = pascal_line(0);
    auto one = pascal_line(1);
    auto four = pascal_line(4);
    auto five = pascal_line(5);
    
    auto expected_for_zero { 1 };
    auto expected_for_one { 1, 1 };
    auto expected_for_four { 1, 4, 6, 4, 1 };
    auto expected_for_five { 1, 5, 10, 10, 5, 1 };
    
    REQUIRE( ranges::equal(zero, expected_for_zero));
    REQUIRE( ranges::equal(one, expected_for_one));
    REQUIRE( ranges::equal(four, expected_for_four));
    REQUIRE( ranges::equal(five, expected_for_five));
}

TEST_CASE("pascal_triangle", "pascal_triangle")
{
    auto pt = std::vector<std::vector<bigint>> {
        { 1 },
        { 1, 1 },
        { 1, 2, 1 },
        { 1, 3, 3, 1 },
        { 1, 4, 6, 4, 1 },
        {1, 5, 10, 10, 5, 1 }
    };
    REQUIRE( ranges::equal(pt, pascal_triangle(5)));
}
