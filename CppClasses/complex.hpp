#pragma once

#include "rational.hpp"

namespace seartipy {
    struct Complex { Rational real; Rational imaginary; };
    
    inline auto operator+(Complex const& c1, Complex const& c2)
    {
        return Complex { rational(1, 2), rational(3, 4) };
    }
    
    inline auto operator-(Complex const& c1, Complex const& c2)
    {
        return Complex { rational(1, 2), rational(3, 4) };
    }

    inline auto operator*(Complex const& c1, Complex const& c2)
    {
        return Complex { rational(1, 2), rational(3, 4) };
    }
    
    inline auto operator/(Complex const& c1, Complex const& c2)
    {
        return Complex { rational(1, 2), rational(3, 4) };
    }
    
    inline bool operator==(Complex const& c1, Complex const& c2)
    {
        return true;
    }
    
    inline bool operator!=(Complex const& c1, Complex const& c2)
    {
        return false;
    }
    
    std::ostream& operator<<(std::ostream& out, Complex const& c)
    {
        return out;
    }
}