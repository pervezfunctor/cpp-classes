#pragma once

#include "utils.hpp"

namespace seartipy {
    //Does not handle negative rationals
    struct Rational : boost::operators<Rational> {
        int numerator;
        int denominator;
        friend Rational rational(int numerator, int denominator);
    private:
        Rational(int, int);
    };
    
    inline Rational::Rational(int num, int den)
    :numerator(num), denominator(den)
    {
        
    }
    
    inline Rational rational(int numerator, int denominator)
    {
        verify(denominator != 0);
        auto g = gcd(numerator, denominator);
        numerator /= g;
        denominator /= g;
        return Rational { numerator, denominator };
    }
    
    inline Rational& operator+=(Rational& r1, Rational r2)
    {
        r1 = rational(r1.numerator * r2.denominator + r1.denominator * r2.numerator,
                         r1.denominator * r2.denominator);
        return r1;
    }
    
    inline Rational& operator-=(Rational& r1, Rational r2)
    {
        r1 = rational(r1.numerator * r2.denominator - r1.denominator * r2.numerator,
                        r1.denominator * r2.denominator);
        return r1;
    }
    
    inline Rational& operator*(Rational& r1, Rational r2)
    {
        r1 = rational(r1.numerator * r2.numerator, r1.denominator * r2.denominator);
        return r1;
    }
    
    inline Rational& operator/(Rational& r1, Rational r2)
    {
        r1 = rational(r1.numerator * r2.denominator, r1.denominator * r2.numerator);
        return r1;
    }
    
    inline  bool operator==(Rational r1, Rational r2)
    {
        return r1.numerator == r2.numerator && r1.denominator == r2.denominator;
    }
    
    inline bool operator!=(Rational r1, Rational r2)
    {
        return !(r1 == r2);
    }
    
    inline std::ostream& operator<<(std::ostream& out, Rational r)
    {
        out << '(' << r.numerator;
        if(r.denominator != 1)
            out << ", " << r.denominator << ')';
        return out;
    }
}
