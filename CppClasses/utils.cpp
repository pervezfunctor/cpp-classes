#include "utils.hpp"

namespace seartipy {
    int gcd(int x, int y)
    {
        if(y == 0)
            return x;
        return seartipy::gcd(y, x%y);
    }
    
    std::vector<std::string> split(std::string line)
    {
        std::vector<std::string> words;
        std::istringstream is(line);
        std::string word;
        while(is >> word)
            words.push_back(word);
        return words;
    }

}